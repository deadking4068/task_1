import time
# Начало подсчета времени
a = time.perf_counter()
def saxpy(a, n):
    s = []
    # Заполнение массивов x и y
    x = [i for i in range(n)]
    y = [i for i in range(n)]
    # Вычисление
    for i in range(0, n):
        s.append(a * x[i] + y[i])
    return s
# Вычислние 25000 раз
z = saxpy(3, 25000)
print(z)
# Конец подсчета времени
f = time.perf_counter() - a
print(f"Время - {f}")

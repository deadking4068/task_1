import random
# Функция для умножения матриц
def calcMatrix(matxF, matxS):
    o = 0  # Число матрицы
    a = []  # Строка матрицы
    t = []  # Конечная матрица
    r = len(matxF)  # Строка 1-ой матрицы
    s = len(matxF[0])  # Столбец 1-ой матрицы
    l = len(matxS[0])  # Столбец 2-ой матрицы
    # Цикл для умножения матриц
    for z in range(0, r):
        for j in range(0, l):
            for i in range(0, s):
                o = o + matxF[z][i] * matxS[i][j]  # Вычисление числа матрицы
            a.append(o)  # Добавление в строку числа
            o = 0
        t.append(a)  # Добавление в матрицу строки
        a = []
    return t
# Создание матриц указанной длины
def createMatrix(rows, cols):
    a = []  # Строка матрицы
    x = []  # Конечная матрица
    # Цикл для заполнения массива
    for i in range(0, rows):
        for j in range(0, cols):
            a.append(random.randint(0, 10))  # Заполнение матрицы числами от 0 до 10

        x.append(a)  # Добавление в матрицу строки
        a = []
    return x
# Вычисление матриц с заданными границами
for i in range(10, 110, 10):
    print(calcMatrix(createMatrix(i, i), createMatrix(i, i)))  # Вывод результата

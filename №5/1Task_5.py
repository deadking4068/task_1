# Метод Гаусса-Жордана
a = [[3, 2, 2], [1, 2, 1]]  # Система уравнений
z = len(a)  # Длина системы
x = [0, 0]  # Результат решения для двойной системы
# Преобразование методом Г-Д.
for i in range(z):
    if int(a[i][i]) == 0:
        break
    for j in range(i + 1, z):
        if i != j:
            ratio = a[j][i] / a[i][i]
            for k in range(z + 1):
                a[j][k] = a[j][k] - ratio * a[i][k]
        else:
            break
x[z - 1] = a[z - 1][z] / a[z - 1][z - 1]  # Первое число
# Нахождение остальных чисел
for i in range(z - 2, -1, -1):
    x[i] = a[i][z]
    for j in range(i + 1, z):
        x[i] = x[i] - a[i][j] * x[j]
    x[i] = x[i] / a[i][i]  # Второе число
print(x)

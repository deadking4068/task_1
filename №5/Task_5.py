# Решение уравнения методом Краммера
a = [[1, 2, 1, 1], [2, 1, 0, 0], [0, 0, 1, 0]]  # Система уравнений
# Вычисление делителя матрицы
def det(mat):
    r = 1  # Конечный результат
    l = len(mat)  # Длина матрицы
    # Получение делителя
    for i in range(l):
        r *= mat[i][i]
        # Получение минора
        for j in range(i + 1, l):
            b = mat[j][i] / mat[i][i]
            mat[j] = [mat[j][k] - b * mat[i][k] for k in range(l)]
    return int(r)
d = []  # Главный определитель системы
q = []  # Строка главного определителя системы
e = []  # Определитель системы
g = []  # Главный столбец матрицы
h = []  # Строка определителя матрицы
c = 0  # Номер столбца определителя
# Вычисление главного определителя
for i in range(0, len(a)):
    for j in range(0, len(a[i]) - 1):
        q.append(a[i][j])  # Получение строк главного определителя
    d.append(q)  # Добавление строки в главный определитель
    q = []
# Вычисление определителя
for z in range(0, len(a)):
    for i in range(0, len(a)):
        g.append(a[i][len(a)])  # Нахождение последних чисел списка
        for j in range(0, len(a[i])):
            if j == c:
                h.append(g[i])  # Добавление главного столбца определителя
                continue
            h.append(a[i][j])  # Добавление чисел во временный массив
        e.append(h)  # Добавление строки в определитель
        h = []
    c += 1  # Увеличение номера столбца
    print(det(e) / det(d))  # Вычисление определителя
    e = []

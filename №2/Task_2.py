import time
x = []  # Хранение кадров
a = open('File_2.md', 'r')
z = ''.join([line for line in a])  # Получение символов
y = 1  # №_кадра
while y < 26:
    frame = z.split(f'\n```\n')  # Разделение кадров
    x.append(str(frame[y]))  # Добавление кадров в массив
    y += 1
print('\n' * 100)
# Вывод анимации 3 раза
for j in range(1, 3):
    for i in range(0, len(x)):
        if i % 2 == 0:
            print(f'\033[31m{x[i]}\033[0m')  # Вывод кадров + изменение цвета шрифта
            time.sleep(1)
            print('\n' * 100)
a.close()
